#!/bin/bash

top -b -n 1 | head -n 12 | tail -n 5 > tmp/processes.info

processes="["

i=0
while read line
do
    owner=$(echo $line | awk '{print $2}')
    cpu=$(echo $line | awk '{print $9}')
    mem=$(echo $line | awk '{print $10}')
    process=$(echo $line | awk '{print $12}')

    if [ ! $i -eq 0 ]; then
        processes=$processes","
    fi

    processes=$processes'{"owner":"'$owner'","cpu":"'$cpu'","mem":"'$mem'","process":"'$process'"}'
    i=$(($i+1))
done < tmp/processes.info

processes=$processes"]"

echo $processes