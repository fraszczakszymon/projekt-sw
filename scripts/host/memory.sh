#!/bin/bash

MEM_BASE=$(free | grep -Eo 'Mem:(\s)+([0-9]*)(\s)+([0-9]*)(\s)+([0-9]*)' | grep -Eo '([0-9]*)(\s)+([0-9]*)(\s)+([0-9]*)')

MEM_TOTAL=$(echo $MEM_BASE | sed -e 's/\([0-9]*\)[ ]*\([0-9]*\)[ ]*\([0-9]*\)/\1/')
MEM_USED=$(echo $MEM_BASE | sed -e 's/\([0-9]*\)[ ]*\([0-9]*\)[ ]*\([0-9]*\)/\2/')
MEM_FREE=$(echo $MEM_BASE | sed -e 's/\([0-9]*\)[ ]*\([0-9]*\)[ ]*\([0-9]*\)/\3/')

SWAP_BASE=$(free | grep -Eo 'Swap:(\s)+([0-9]*)(\s)+([0-9]*)(\s)+([0-9]*)' | grep -Eo '([0-9]*)(\s)+([0-9]*)(\s)+([0-9]*)')

SWAP_TOTAL=$(echo $SWAP_BASE | sed -e 's/\([0-9]*\)[ ]*\([0-9]*\)[ ]*\([0-9]*\)/\1/')
SWAP_USED=$(echo $SWAP_BASE | sed -e 's/\([0-9]*\)[ ]*\([0-9]*\)[ ]*\([0-9]*\)/\2/')
SWAP_FREE=$(echo $SWAP_BASE | sed -e 's/\([0-9]*\)[ ]*\([0-9]*\)[ ]*\([0-9]*\)/\3/')

printf '{"memory":{"total":%s,"used":%s,"free":%s},"swap":{"total":%s,"used":%s,"free":%s}}\n' $MEM_TOTAL $MEM_USED $MEM_FREE $SWAP_TOTAL $SWAP_USED $SWAP_FREE