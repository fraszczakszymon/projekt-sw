#!/bin/bash

if [ ! -f "tmp/cpu.info" ]; then
    cat /proc/stat | grep '^cpu[0-9]' > tmp/cpu.info
fi
cat /proc/stat | grep '^cpu[0-9]' > tmp/curCpu.info

numberOfCpus=$(wc -l tmp/cpu.info | awk '{print $1}')
cores="["

i=0
while read line
do
    cpu1=$(echo $line | awk '{print $2}')
    cpu2=$(echo $line | awk '{print $3}')
    cpu3=$(echo $line | awk '{print $4}')
    cpu4=$(echo $line | awk '{print $5}')
    cpu5=$(echo $line | awk '{print $6}')
    cpu6=$(echo $line | awk '{print $7}')
    cpu7=$(echo $line | awk '{print $8}')
    cpu8=$(echo $line | awk '{print $9}')
    cpu9=$(echo $line | awk '{print $10}')
    cpu10=$(echo $line | awk '{print $11}')

    total[$i]=$(($cpu1+$cpu2+$cpu3+$cpu4+$cpu5+$cpu6+$cpu7+$cpu8+$cpu9+$cpu10))
    usage[$i]=$(($cpu1+$cpu2+$cpu3))
    i=$(($i+1))
done < tmp/cpu.info

i=0
while read line
do
    cpu1=$(echo $line | awk '{print $2}')
    cpu2=$(echo $line | awk '{print $3}')
    cpu3=$(echo $line | awk '{print $4}')
    cpu4=$(echo $line | awk '{print $5}')
    cpu5=$(echo $line | awk '{print $6}')
    cpu6=$(echo $line | awk '{print $7}')
    cpu7=$(echo $line | awk '{print $8}')
    cpu8=$(echo $line | awk '{print $9}')
    cpu9=$(echo $line | awk '{print $10}')
    cpu10=$(echo $line | awk '{print $11}')

    currTotal[$i]=$(($cpu1+$cpu2+$cpu3+$cpu4+$cpu5+$cpu6+$cpu7+$cpu8+$cpu9+$cpu10))
    currUsage[$i]=$(($cpu1+$cpu2+$cpu3))
    i=$(($i+1))
done < tmp/curCpu.info

for (( i=0; i<$numberOfCpus; i++ ))
do
    cpuUsage=$(( 100 * (${currUsage[$i]} - ${usage[$i]}) / (${currTotal[$i]} - ${total[$i]})  ))
    if [ ! $i -eq 0 ]; then
        cores=$cores","
    fi
    cores=$cores$cpuUsage
done

cp tmp/curCpu.info tmp/cpu.info

echo $cores"]"

#grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage "%"}'