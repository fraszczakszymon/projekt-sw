var exec = require('exec-sync');

module.exports = {
    getJson: function () {
        return JSON.stringify({
            hostname: exec("hostname"),
            ip: exec("./scripts/host/ip.sh"),
            os: exec("uname -o"),
            kernel: exec("uname -r"),
            uptime: exec("uptime -p"),
            dateTime: exec("date"),
            arch: exec("uname -m")
        });
    }
}
