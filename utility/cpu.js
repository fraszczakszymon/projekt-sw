var exec = require('exec-sync'),
    output = {
        cpuModel: exec('cat /proc/cpuinfo | grep "model name" -m 1 | cut -d ":" -f 2- | cut -d " " -f 2- '),
        usage: [],
        processes: []
    };

module.exports = {
    getJson: function () {
        output.usage = JSON.parse(exec('./scripts/host/cpu.sh'));
        output.processes = JSON.parse(exec('./scripts/host/processes.sh'));

        return JSON.stringify(output);
    }
}