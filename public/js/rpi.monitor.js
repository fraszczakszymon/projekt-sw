var rpi = rpi || {};

rpi.monitor = (function ($) {
    var cpuChart,
        ramChart,
        host = function () {
            $.get("./host", function (data) {
                var host = data;

                host.uptime = host.uptime.replace('up ', '').replace('hours', 'godz').replace('minutes', 'min');
                for (key in host) {
                    $('#' + key).html(host[key]);
                }
            });
        },
        cpu = function () {
            $.get("./cpu", function (data) {
                var cpu = data,
                    processes = $('#processes'),
                    row,
                    cpuCanvas = document.getElementById("cpuChart").getContext("2d");

                processes.empty();
                for (process in cpu.processes) {
                    row = $('<tr/>');
                    row.append($('<td/>').text(cpu.processes[process].process));
                    row.append($('<td/>').text(cpu.processes[process].owner));
                    row.append($('<td/>').text(cpu.processes[process].cpu + '%'));
                    row.append($('<td/>').text(cpu.processes[process].mem + '%'));
                    processes.append(row);
                }

                $('#cpuModel').text(cpu.cpuModel);

                cpuChart = new Chart(cpuCanvas).Bar({
                    labels : ["CPU 1"],
                    datasets : [
                        {
                            fillColor : "rgba(100,100,170,0.5)",
                            strokeColor : "rgba(100,100,170,0.7)",
                            data : cpu.usage
                        }
                    ]
                }, {
                    scaleOverride : true,
                    scaleSteps : 5,
                    scaleStepWidth : 20,
                    scaleStartValue : 0,
                    animation : false
                });
            });
        },
        memory = function () {
            $.get("./memory", function (data) {
                var memory = data,
                    total = $('#memoryTotal'),
                    ramCanvas = document.getElementById("ramChart").getContext("2d");

                total.text(Math.round(memory.memory.total / 1000) + "MB");

                ramChart = new Chart(ramCanvas).Doughnut([{
                        value: memory.memory.used,
                        color:"#F7464A"
                    },
                    {
                        value : memory.memory.free,
                        color : "#4D5360"
                    }], {animation : false});
            });
        },
        diodes = function () {
            $('.diode').each(function () {
                var that = this;

                $.get("./gpio/get/" + $(that).attr('data-pin'), function (data) {
                    var status = data === "0" ? "0" : "1";

                    if (status === "1") {
                        $(that).removeClass('btn-default').addClass('btn-danger');
                        $(that).attr('data-status', '1');
                    } else {
                        $(that).removeClass('btn-danger').addClass('btn-default');
                        $(that).attr('data-status', '0');
                    }
                })
            });
        };

    return {
        renderHost: function () {
            host();
        },
        renderCpu: function () {
            cpu();
        },
        renderMemory: function () {
            memory();
        },
        refreshDiodes: function () {
            diodes();
        }
    };
})($);
