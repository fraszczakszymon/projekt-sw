$(document).ready(function () {
    rpi.monitor.renderHost();
    rpi.monitor.renderCpu();
    rpi.monitor.renderMemory();
    rpi.monitor.refreshDiodes();

    setInterval(function () {
        rpi.monitor.renderHost();
        rpi.monitor.renderCpu();
        rpi.monitor.renderMemory();
    }, 1000);

    $('.diode').on('click', function () {
        var status = $(this).attr('data-status') === "0" ? "1" : "0";

        $.get("./gpio/set/" + $(this).attr('data-pin') + '/' + status, function () {});
        rpi.monitor.refreshDiodes();
    });

    $('#dateButton').on('click', function () {
        var date = $('#date').val(),
            pin = $('#datePin').val();

        $.get('./schedule/' + date + '/' + pin, function () {
            $('#date').val('');
        });
    });
});
