var express = require('express'),
    fs = require('fs'),
    http = require('http'),
    host = require('./utility/host'),
    cpu = require('./utility/cpu'),
    memory = require('./utility/memory'),
    exec = require('exec-sync'),
    app = express();

app.use(express.compress());
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res, next) {
    fs.readFile('app/dashboard.html', function(err, data) {
        res.set('Content-Type', 'text/html');
        res.send(data);
        next();
    });
});

app.get('/host', function (req, res, next) {
    res.set('Content-Type', 'application/json');
    res.send(host.getJson());
    next();
});

app.get('/cpu', function (req, res, next) {
    res.set('Content-Type', 'application/json');
    res.send(cpu.getJson());
    next();
});

app.get('/memory', function (req, res, next) {
    res.set('Content-Type', 'application/json');
    res.send(memory.getJson());
    next();
});

app.get('/gpio/get/:pin', function (req, res, next) {
    var pin = req.params.pin;

    if (pin == 17 || pin == 27 || pin == 22 || pin == 23 || pin == 24) {
        res.send(exec('cat /sys/class/gpio/gpio' + pin + '/value') + "");
    }
    next();
});

app.get('/gpio/set/:pin/:status', function (req, res, next) {
    var pin = req.params.pin,
        status = req.params.status;

    if (pin == 17 || pin == 27 || pin == 22 || pin == 23 || pin == 24) {
        exec('./scripts/set.sh ' + pin + ' ' + status);
        res.send("OK");
    }
});

app.get('/schedule/:date/:pin', function (req, res, next) {
    var date = req.params.date,
        pin = req.params.pin;

    exec('./scripts/cron.sh "' + date + '" ' + pin);
    res.send("OK");
    next();
});

app.listen(8888, function () {
    console.log('Server is listening on http://localhost:8888/')
});
